# Flyer en ligne


Lien version en ligne: https://eval_julien.surge.sh/

Lien maquette: https://www.figma.com/file/SGBU9wcRVNI49DSdKXqT4B/flyer-en-ligne?node-id=0%3A1


Flyer en ligne pour un concours de pétanque. 

- Le flyer est sur une page, qui comporte toute les modalités concernant le concours et un lien vers l'adresse mail du coordinateur de l'événement.
